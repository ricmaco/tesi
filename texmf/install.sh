#!/usr/bin/env bash

mkdir -p /usr/share/texmf-dist/tex/latex/tesi
sudo cp tesi.sty /usr/share/texmf-dist/tex/latex/tesi/
sudo texhash >/dev/null 2>&1
sudo mktexlsr >/dev/null 2>&1
